#ejemplo 1 de funciones de orden superior
def llamada(f):
     print("aqui1:",f.__name__)
     print("aqui2:",type(f.__name__))
     return f() 
    
def saludar():
    return "hola como estas"

print( llamada(saludar) )

#ejemplo 2 funciones de orden superior con parametros

def operaciones(x,f):
    return f(x)

#funcion de devolucion de raiz cuadrada de un numero dado
def sqrt(x):
    return x**0.5
#funcion de devolucion al cuadrado de un numero dado
def squared(x):
    return x**2

print("operacion 1:", operaciones(36,sqrt) )
print("operacion 2:", operaciones(7,squared) )    

def ejecucion(f,a,b):
    return f(a,b)


def suma(a,b):
    return a+b

print("la suma:", ejecucion(suma,10,5) )    


#Funcion Filter filter(function,iterable)
numeros = [1,2,3,4,5,6,7,8,9,10]
print("lista numeros:",numeros)
#devolver una lista de numeros pares
filtrado = list( filter( lambda x:x%2==0, numeros ) )

print("lista nueva:",filtrado)


#Funcion Map
#map(function,iterable)
num = [1,2,3,4,5,6,7,8,9,10,11,12]
print("lista_num:",num)
num_cubo = list( map( lambda x:x**3, num ) )
print("lista_cubo:",num_cubo)

#Funcion Reduce- reduce(function,iterable) 
#debemos importar el modulo
from functools import reduce

number=[1,2,3,4,5,6,7,8,9,10]
print("number-reduce:",number)
sumar_todo = reduce(lambda a,b:a+b, number)
print("reduce:",sumar_todo) 
