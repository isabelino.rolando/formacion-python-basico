#lista sintaxis
#las posiciones de las listas inician en 0
lista = [1,2,3,4,5]
print(lista)
print("tercera posicion "+str(lista[2]))

lista2 = list("6789")
print(lista2)
print("ultima posicion "+str( lista2[3]) )

lista3 = [ 1,"Hola",3.60,[1,2,3] ]
print(lista3)
print(lista3[3][0])

#modificar listas
a = [90,"Python",3.87]
print(a)
a[2] = 100
print(a)
#eliminar contenido de lista del 
del a[0]
print(a)
#agregar nuevo elemento
a.append("Codigo")
print(a)

#Tuplas, sus valores son inmutables
mitupla = (1,2,3,4,5,6)
#mitupla[2]=50
print(mitupla[3])

#Set
myset = {"manzana","freza","naranja"}
instrumentos = set(["Guitarra","Bateria","Bajo"])
print(instrumentos)
