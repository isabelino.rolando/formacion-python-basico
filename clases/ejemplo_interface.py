#clase de tipo interface
class Mando:
    def siguiente_canal(self):
        pass

    def canal_anterior(self):
        pass

    def subir_volumen(self):
        pass

    def bajar_volumen(self):
        pass   


class MandoSamsung(Mando):
    def siguiente_canal(self):
        print("Samsung->Siguiente")

    def canal_anterior(self):
        print("Samsung->Anterior")

    def subir_volumen(self):
        print("Samsung->subir volumen")

    def bajar_volumen(self):
        print("Samsung->bajar volumen")           


mando = MandoSamsung()
mando.siguiente_canal()
mando.canal_anterior()
mando.subir_volumen()
mando.bajar_volumen()        