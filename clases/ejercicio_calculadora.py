class Calculadora:
    #atributos
    num1=0
    num2=0

    #constructor
    def __init__(self,n1,n2):
        self.num1 = n1
        self.num2 = n2
           

    def suma(self):    
        return f"La suma es: {(self.num1+self.num2)}"

    def resta(self):    
        return f"La resta es:{(self.num1-self.num2)}"

    def multiplicacion(self):    
        return f"La multiplicación es:{(self.num1*self.num2)}"

    def division(self):    
        return f"La división es:{(self.num1/self.num2)}"

    def realizarOperacion(self,operacion):
        
        if operacion=="+":
           return self.suma()
        elif operacion=="-":
            return self.resta()
        elif operacion=="*":
            return self.multiplicacion()
        elif operacion=="/":
            return self.division()            
        

#programa
ejecutar = True

while ejecutar:

    n1=float(input("Ingrese el primer numero: "))
    op=input("Que operacion +(suma),-(resta),*(multiplicacion),/(division): ")
    n2=float(input("Ingrese el segundo numero: "))
    
    calculadora = Calculadora(n1,n2)
    print(calculadora.realizarOperacion(op))
    
   

    valor=input("Desea seguir o terminar s/t: ")    
    if valor=="t":
        ejecutar=False
        print("fin del programa")