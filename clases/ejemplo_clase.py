#ejemplo de clase
class Perro:
    #atributos o variables de clase
    tipo_animal="canino"
    raza = ""
    edad = 0
    color = ""
    nombre = ""
    
    #constructor una funcion o metodo que se ejecuta
    #al instanciar o crear un objeto de la clase
    def __init__(self,raza,edad,color,nombre):
        self.raza = raza
        self.edad = edad
        self.color= color
        self.nombre = nombre
            

    def mostrarTodo(self):
        print(f"Nombre:{self.nombre}\nRaza:{self.raza}\nColor:{self.color}\nEdad:{self.edad}")

    def ladrar(self):
        print("wooff wooff wooff")

    def comer(self,comida):
        print(f"comiendo {comida} ....")    





#instanciar la clase Perro
#crear un objecto de la clase Perro
perro1 = Perro("caniche",4,"blanco","pupi")
perro1.mostrarTodo()
perro1.ladrar()
perro1.comer("pollo")

print("===================================")
perro2 = Perro("labrador",5,"marron","tarzan")
perro2.mostrarTodo()
perro2.ladrar()
perro2.comer("carne vacuna")

