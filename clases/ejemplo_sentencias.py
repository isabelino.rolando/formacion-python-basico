#sentencia break se utiliza cuando queremos finalizar
#   un ciclo while o for


#while
i = 0
while True:
    i += 1
    print(i)
    if i > 5:
        break
#for
print("#####for")
for num in range(0,9):
    print(num)

    if num == 5:
        break

#sentencia continue, se utiliza para obviar una parte de codigo dentro
# de un bucle
print("#############Continue")

laboral = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo']
finde = ['sabado','domingo']

for dia in laboral:

    if dia in finde:
        print(f"Hoy es {dia}, no se trabaja")
        continue

    print(f"Hoy es {dia}, toca trabajar")



#Setencia pass, se utiliza para clase,metodo o funcion, bucle o condional
#cuando todavia no queremos definir ningun comportamiento dentro

class MyClass:
    pass

class MyClass2:
    def my_method(self):
        pass

def my_function():
    pass


if 2==2:
    pass


while True:
    pass

for num in range(1,5):
    pass
