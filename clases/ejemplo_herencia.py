class Insecto:
    tipo=""
    def __init__(self,tipo):
        self.tipo= tipo

    def categoria(self,categoria):
        print(f"Es de categoria {categoria}")    


class Animal:
    especie=""
    edad=""
    def __init__(self,especie,edad):
        self.especie = especie
        self.edad = edad

    def comunicarse(self):
        #Metodo vacio
        pass

    def moverse(self):
        #metodo vacio
        pass

    def descripcion(self):
        print("Soy un animal del tipo", type(self).__name__)

#Perro hereda de Animal
#Perro es una clase hija de animal
#Animal es una clase padre de Perro
class Perro(Animal):
    raza=""
    nombre=""
    color=""
    def __init__(self, especie, edad,raza,nombre,color):
        super().__init__(especie, edad)
        self.raza=raza
        self.nombre= nombre 
        self.color=color   
    #heredo los metodos y puedo sobre escribirlos
    def comunicarse(self):
        print("ladrando")

    def moverse(self):
        print("caminando en 4 patas")
#objecto de la clase Perro
perro1 = Perro("canino",5,"labrador","Lazie","blanco")
print("Especie:",perro1.especie)
print("Edad:",perro1.edad)
print("Nombre:",perro1.nombre)
print("Raza:",perro1.raza)
print("Color:",perro1.color)
perro1.moverse()
perro1.comunicarse()
perro1.descripcion()


class Gato(Animal):
    def comunicarse(self):
        print("maulla")
    #def moverse(self):
    #    print("Camina en 4 patas")


class Abeja( Animal,Insecto):
    tipo_abeja=""
    def __init__(self, especie, edad,tipo,tipo_abeja):
        Animal.especie= especie
        Animal.edad= edad
        Insecto.tipo = tipo
        self.tipo_abeja = tipo_abeja        
        


    def comunicarse(self):
        print("sumbidos")
    def moverse(self):
        print("volando")    

print("#######################")
objGato = Gato("Felino",4)
objGato.descripcion()
objGato.comunicarse()
objGato.moverse()
print("#######################")

objAbeja = Abeja("insecto",1,"Insecto Volador","obrero")
objAbeja.tipo="Insecto Volador"
objAbeja.categoria("volador")
objAbeja.descripcion()
objAbeja.comunicarse()
objAbeja.moverse()