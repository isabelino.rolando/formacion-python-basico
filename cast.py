#cast implicito
a = 1#int
b = 2.3#float

print(type(a))
#print(type(b))
a = a + b

print(type(a))

#cast explicito
print("conversiones")
#convertir float a int
n=3.5
print(type(n))#float
n = int(n)
print(type(n))#int
print(n)

#convertir int a float
m=100
print(type(m))#int
m = float(m)
print(m)
print(type(m))#float

#convertir cualquier valor a string
p=3.14
p=str(p)
print(p)
print(type(p))
#convertir string a float
num1 = "3.568"
num1 = float(num1)
print(type(num1))
#convertir string a int
num2 = "300"
num2 = int(num2)
print(type(num2))

#convertir bolean en int False es 0, True es 1

dato = False
print(type(dato))
dato = int(dato)
print(type(dato))
print(dato)
#convertir set a lista
listaSet = {1,2,3,4}
print(type(listaSet))
lista2 = list(listaSet)
print(type(lista2))

#convertir tupla a lista

listaTup = (1,2,3,4)
print(type(listaTup))
lista2 = list(listaTup)
print(type(lista2))

#lista a set
w= ["Python","Javascript","C++"]
q= set(w)

#lista a tupla
z =tuple(w)

set1= {1,2,3}
print(type(set1))
tup1= tuple(set1)
print(type(tup1))
