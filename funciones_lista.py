#funciones de listas mas importantes
dia_de_semana = ["Lunes","Martes","Miercoles","Jueves","Viernes"]
print("principal",dia_de_semana)
#append() es una funcion para agregar un elemento al final de la lista
dia_de_semana.append("Sabado")
print("append",dia_de_semana)
#insert(posicion,elemento) inserta un elemento en una posicion particular
dia_de_semana.insert(6,"Domingo")
print("insert",dia_de_semana)
#remove() elimina un elemento de lista recibiendo el parametro especifico
dia_de_semana.remove("Domingo")
print("remove",dia_de_semana)

#sort ordenar la lista en orden ascendente
lista_numeros=[7,1,2,8,9,5,6,4,3,7]
print("lista_numeros",lista_numeros)
lista_numeros.sort()
print("sort",lista_numeros)

#ordernar la lista en orden descendente
#lista_numeros.sort(reverse=True)
#print("sort_reverse",lista_numeros)

#sorted una funcion que devuelve una lista dada en orden descendente

print("sorted", sorted(lista_numeros,reverse=True) )


#extend() agregar varios elementos a una lista
lista_numeros.extend([20,15,30,55])

print(lista_numeros)

#index() devuelve la posicion de un elemento dado
print("index",lista_numeros.index(1))

#max() devuelve el elemento de valor maximo en una lista - para valores numericos
print("max", max(lista_numeros) )

#min() devuelve el elemento de valor minimo en una lista- para valores numericos
print("min",min(lista_numeros) )

#len() devuelve la longitud total de una lista
print( "len",len(lista_numeros) )

#count() devuelve el recuento de cuatas veces aparece un objeto en una lista dada
print( "count",dia_de_semana.count("Martes") )

#reverse() invierte el orden de la lista
print(dia_de_semana)
dia_de_semana.reverse()
print("reverse",dia_de_semana)
print(lista_numeros)
lista_numeros.reverse()
print("reverse",lista_numeros)

#copy() devuelve un duplicado de una lista

dias_laborales=dia_de_semana.copy()
print("copy",dias_laborales)

#pop() elimina un elemento dada la posicion
dias_laborales.pop(0)
print("pop",dias_laborales)

#clear() elimina todos los elementos de una lista
dias_laborales.clear()
print("clear",dias_laborales)



#Obtener el ultimo elemento de lista_numeros, teniendo en cuenta
#que no se su longitud total


#obtener la ultima posicion
ultimo_valor = lista_numeros[ len(lista_numeros) - 1 ] 
print("ultimo valor",ultimo_valor)