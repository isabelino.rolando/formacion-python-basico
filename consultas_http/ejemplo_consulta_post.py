import requests

url="http://httpbin.org/post"

datos = {
    'lenguaje':'Python',
    'version':'3.10.0'
}

header={
    'User-Agent':'PythonAgent',
    'Agent-Version':'1.0.0'
}

respuesta = requests.post(url,data= datos, headers=header)

texto_respuesta= respuesta.text
print(texto_respuesta)