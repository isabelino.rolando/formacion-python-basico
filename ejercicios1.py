#ejercicio 1
'''
valor1 =int(input("Ingrese el valor 1: "))
valor2 =int(input("Ingrese el valor 2: "))
valor3 =int(input("Ingrese el valor 3: "))

if valor1>valor2 and valor1>valor3:
    print("el valor 1 es el mayor: "+str(valor1))
elif valor2>valor1 and valor2>valor3:
    print("el valor 2 es el mayor: "+str(valor2))
elif valor3>valor1 and valor3>valor2:
    print("el valor 3 es el mayor: "+str(valor3))
'''

#ejercicio 2
'''
valorNumerico = int(input("Ingrese el valor numerico: "))

if valorNumerico >= 0:
   print("el numero es positivo ",valorNumerico)  
else:
    print("el numero es negativo ",valorNumerico) 
'''
#ejercicio 3
'''
valorNumerico = int(input("Ingrese el valor numerico: "))

if valorNumerico == 0:
    print("el numero no es par, ni impar",valorNumerico)  
else:
    if valorNumerico%2 == 0:
        print("el numero es par ",valorNumerico)  
    else:
        print("el numero es impar ",valorNumerico)
'''
#ejercicio 4
'''
usuarios = [['usu1','pas1'],['usu2','pas2'],['usu3','pas3']]

user = input("Ingrese el usuario: ")
password = input("Ingrese el password: ")

if ((user==usuarios[0][0] and password==usuarios[0][1]) or
    (user==usuarios[1][0] and password==usuarios[1][1]) or
    (user==usuarios[2][0] and password==usuarios[2][1]) ):

    print("Bienvenido "+user )
else:
    print("Credenciales incorrectas")    

'''

#ejercicio 5
terminar=True

while terminar:

    x =float(input("Ingrese el valor x: "))
    y =float(input("Ingrese el valor y: "))

    if x>0 and y>0:
        print( "Primer cuadrante "+"x="+str(x)+","+"y="+str(y) )
    elif x>0 and y<0:
        print("Cuarto cuadrante "+"x="+str(x)+","+"y="+str(y) ) 
    elif x<0 and y<0:
        print("Tercer cuadrante "+"x="+str(x)+","+"y="+str(y) )
    elif x<0 and y>0:
        print("Cuarto cuadrante "+"x="+str(x)+","+"y="+str(y) )
    else:
        print("Origen "+"x="+str(x)+","+"y="+str(y) )

    print("Desea continuar con el programa?")
    valor = input("Ingrese S o N: ")


    if valor == "N":
        print("fin del programa")
        terminar=False
      


#En los 5 ejercicios de ayer agregar bucles para que
#cada uno de los programas se siga ejecutando 
#una y otra vez hasta que tu con un parametro puedas cerrarlo