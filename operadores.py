#Operadores de Asignación
print("Operadores de Asignación")
a = 7; b = 2
x = a; x += b;print("x+=",x)
x = a; x -= b;print("x-=",x)
x = a; x *= b;print("x*=",x)
x = a; x /= b;print("x/=",x)
x = a; x %= b;print("x%=",x)
x = a; x //= b;print("x//=",x)
x = a; x **= b;print("x**=",x)

#and bit a bit
x = a; x &= b;print("x&=",x)
#or bit a bit
x = a; x |= b;print("x|=",x)
#or exclusivo bit a bit
x = a; x ^= b;print("x^=",x)
#desplaza x b bits a la derecha
x = a; x >>= b;print("x>>=",x)
#desplaza x b bits a la izquierda
x = a; x <<= b;print("x<<=",x)

#Operadores Aritméticos
print("#Operadores Aritméticos")
k = 10 ; l = 3
print("k+l= ",k+l)
print("k-l= ",k-l)
print("k*l= ",k*l)
print("k/l= ",k/l)
print("k%l= ",k%l)#modulo(resto de division)
print("k**l= ",k**l)#potencia
print("k//l= ",k//l)#division entera

#Operadores Relacionales
print("Operadores Relacionales")
m = 2; n = 3
print("m == n = ",m == n)#igualdad
print("m != n = ",m != n)#desigualdad
print("m > n = ",m > n)#mayor
print("m < n = ",m < n)#menor
print("m >= n = ",m >= n)#mayor o igual
print("m <= n = ",m <= n)#menor o igual

#Operadores Lógicos
#and devuelve un True si ambos elementos son True
#or devuelve un True si al menos un elemento es True
#not devuelve lo contrario del valor, si es True es False y viceversa
print("operador and")
print(True and True)#True
print(True and False)#False
print(False and True)#False
print(False and False)#False

print("operador or")
print(True or True)#True
print(True or False)#True
print(False or True)#True
print(False or False)#False

print("operador not")
print(not True)#False
print(not False)#True


