#estructura repetitiva while - se repite el bloque de codigo mientras sea verdadero

x=5 
while x > 0:
    print("ejecucion")
    #contador aumentar o disminuir mi dato de expresion dado, x
    x = x - 1
    print(x)

y = 1
while y <= 100:
    print(y)
    y = y + 1
print("#while con else")
# while con else
q=-1
while q>0:
    q = q - 1
    print(q)#4,3,2,1,0
else:
    print("El bucle ha finalizado")     

#recorrer lista con while

lista1 = ["Lunes","Martes","Miercoles","Jueves","Viernes"]

indice = 0
limite = len(lista1) - 1

while indice <= limite:
    print( lista1[indice] )
    indice += 1

print("bucle for")
#bucle for
#range(inicio,fin,salto)
print("for-ejemplo 1")
for i in range(50,101):
    print(i)
#ejemplo2
print("for-ejemplo 2")
for n in [1,2,3,4,5]:
    print(n)
print("for-ejemplo 3")
for dias in lista1:
    print(dias)    

print("for-ejemplo 4")
for day in range(0,len(lista1)):
    print( lista1[day] ) 



