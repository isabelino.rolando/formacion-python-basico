#sintaxis de variables a la hora de declarar
#diferencias entre mayusculas y minusculas
#nombre de variables no pueden empezar por un numero
#no se permite el uso de guiones -
#no se permite el uso de espacios en blanco

#declaraciones validas
_variable = 10
vari_able = 20
variable3 = 30
variable = 60
variaBle = 70

#declaraciones no validas
#2variable = 10
#varia-ble = 20
#varia ble = 30

#no se pueden usar palabras reservadas para nombrar variables
#ver las palabras reservadas que tenga con keyword
import keyword
print(keyword.kwlist)

#uso de parantesis
x = 10
y = (x*3-3)**(10-2)+3

print(y)
