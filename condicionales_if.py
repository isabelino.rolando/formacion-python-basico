#condicional if

edad = 17
nombre = "Pepe"

if edad != 18:
    print("tienes 18 años")

print("fin del programa")

#condicional if else

if edad == 18:
    print("tienes 18")
else:
    print("no tienes 18")    

#condicional elif

if edad ==18:
    print("Tienes 18")
elif edad > 18:
    print("Eres mayor a 18")
elif edad < 18:
    print("Eres un chaval")
          

if (edad == 18 or nombre == "Pepe"):
    print("Bienvenido a la fiesta")
else:
    print("no eres bienvenido")


#operador ternario-ifelse en una sola linea de codigo

x = 6
#respuesta = "Es 5" if x == 5 else "No es 5"
print("Es 5" if x == 5 else "No es 5")
