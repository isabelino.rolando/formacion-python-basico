'''
def generarMultiplos5(limite):
    listaNumeros=[]

    for i in range(1,limite):
        listaNumeros.append(i*5)

    return listaNumeros

print( generarMultiplos5(11) )  
'''
#yield generar un objeto iterable
def generarMultiplos5(limite):

    for i in range(1,limite):
        yield i*5


objeto = generarMultiplos5(11)#objeto de tipo generator

#mi_lista = list(objeto)#el cast a list para mostrarlo normal
#print(type(mi_lista))
#print("mi lista",mi_lista)

for n in objeto:#esto es para mostrar los datos de tipo generator
    print(n)


