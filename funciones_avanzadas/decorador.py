
def revisar(f):
    def control(a,b):
        if b == 0:
           return  "No se puede dividir entre 0"
        
        return f(a,b)
    return control    

@revisar
def dividir(a,b):
    return a/b

#dividir =  revisar(dividir)   

print(dividir(10,0))    