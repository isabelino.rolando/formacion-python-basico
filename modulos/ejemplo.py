#utilizacion de modulos internos
import math
import mimodulo as oper
import otros.calculo as calc
from clases import ejemplo_clase as p

perrito = p.Perro("dalmata",4,"blanco","Fido")

print(perrito.mostrarTodo())
print("mimodulo", oper.multiplicacion(10,2))

print("math", math.factorial(10) )

print("calculo",calc.cuadrado(9))
