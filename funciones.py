#funciones
#por convension los nombres de funciones van en minuscula
def saludo(nombre):
    print("Hola, buenos dias %s..."%nombre)

#llamada de la funcion
#saludo( input("Ingrese el nombre para el saludo:") )

#funcion de retorno
def resta(a,b):
    return a-b

valor = resta(10,5)
print(type(valor))
print("La resta es: ",valor)

#valor por defecto
def credenciales(user,password,tipo="normal"):
    return "Bienvenido usuario: "+str(user)+" eres de tipo "+str(tipo)

print(credenciales("Jose","jose123"))

#uso de *args poder definir multiples parametros de entrada
print("#args")
def suma(*args):
    s = 0
    for arg in args:
        s += arg
    
    
    return s 

print( suma(10,25,15) )
#uso de *kwargs definir multiples parametros de entrada con asignacion
def suma2(**kwargs):
    t = 0
    for key, value in kwargs.items():
        print(key,"=",value)
        t += value

    return t    

print(suma2(a=3,b=10,c=5,d=2))    

#anotaciones en funciones
#asignando tipo de variable de entrada
def suma3( a:int,b:int ) -> int:
    return a+b

print(type(suma3(10,5)))
print( "la suma es: "+str(suma3(10,5)) )

#funciones lambda o anonimas, se define en una sola linea
#siempre debe de retornar valor

sumatoria = lambda a,b : a+b

print("la sumatoria es",sumatoria(5,3))


#return de multiples variables o valores
def valoresAlcuadrado(x,y):
    valorXcuadrado=x*x
    valorYcuadrado=y*y
    return valorXcuadrado,valorYcuadrado

resultadoX , resultadoY = valoresAlcuadrado(5,8)
print("cuadrado X:",resultadoX)
print("cuadrado Y:",resultadoY)

def datoMinSeg(x):
    hs = x/3600
    min = (x % 3600)/60
    seg = (x % 3600)% 60

    return hs,min,seg

h,m,s = datoMinSeg(3660)

print("hora: ",h)
print("minuto: ",m)
print("segundo: ",s)

