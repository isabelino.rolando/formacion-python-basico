#importamos la libreria de python con mysql
import pymysql

try:
    conexion = pymysql.connect( host='localhost',
                                user='root',
                                password='',
                                db='database_python')
    print("Conexión exitosa!")

    # consulta a tabla
    '''
    try:
        with conexion.cursor() as cursor:
            
            cursor.execute("select nombre,apellido,email from persona;")
            print("rowcount:",str(cursor.rowcount))
            print("rownumber:",str(cursor.rownumber))
            datos_persona = cursor.fetchall()

            print("cantidad de registros:",len(datos_persona) )
            print(type(datos_persona))


            for p in datos_persona:
                print(p)
            
            #number_of_rows= cursor.execute("select * from persona")
            #print("Cantidad de registros en persona: ",number_of_rows)
    finally:
        conexion.close()
  
    
    '''
    # insertar datos
    '''
    try:
        with conexion.cursor() as cursor:
            consulta = "insert into persona(nombre,apellido,email) values(%s,%s,%s);"

            #cursor.execute(consulta,("pepe","perez","pp@email.com") )
            #cursor.execute(consulta,("ramon","soto","rs@email.com") )
            cursor.execute(consulta,("Orlando8","Tuyo","ot@email.com") )
            cursor.execute(consulta,("Orlando9","Tuyo","ot@email.com") )
            cursor.execute(consulta,("Orlando10","Tuyo","ot@email.com") )

        conexion.commit()
        print("Registro correcto!")



        #number_of_rows= cursor.execute("select * from persona")
        #print("Cantidad de registros en persona: ",number_of_rows)
    finally:
        conexion.close()
    
    '''
    #actualizar datos
    '''
    try:

        with conexion.cursor() as cursor:
            consulta = "update persona set nombre=%s,apellido=%s,email=%s where id=%s"
            newNombre= "Tito"
            newApellido = "Villanova"
            newEmail = "titov@email.com"
            id=4

            cursor.execute(consulta,(newNombre,newApellido,newEmail,id))

            conexion.commit()

            print("Actualización correcta!")

    finally:
        conexion.close()
        '''

    #eliminar registro
    '''
    try:
        with conexion.cursor() as cursor:
            #consulta= "delete from persona where nombre like %s"
            consulta= "delete from persona where id=%s"
            valor="Orlando%"
            id=12
            cursor.execute(consulta,id)

            conexion.commit()
            print("Registro borrado correctamente!")
    finally:
        conexion.close()
     '''
    #consulta con where

    try:
        with conexion.cursor() as cursor:
           consulta= "select nombre,apellido,email from persona where apellido=%s"
           cursor.execute(consulta,("Perez"))
           datos = cursor.fetchall()
           for d in datos:
            print(d)
    finally:
        conexion.close()


except (pymysql.err.OperationalError, pymysql.InternalError) as e:
    print("Ocurrio un error: ",e)  



     

