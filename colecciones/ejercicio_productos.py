class Producto:
    id=0
    categoria=""
    marca=""
    descripcion=""
    precio=0
    cantidad=0
    def __init__(self,id,categoria,marca,descripcion,precio,cantidad):
        self.id=id
        self.categoria=categoria
        self.marca=marca
        self.descripcion=descripcion
        self.precio=precio
        self.cantidad=cantidad

    def verProducto(self):
        return f"id:{self.id}, categoria:{self.categoria}, marca:{self.marca}, descripcion:{self.descripcion}, precio:{self.precio}, cantidad:{self.cantidad}"    

contador_producto=0
#creacion de un diccionario donde guardaremos los objectos de Producto
db_producto=dict([])

def nuevoProducto(id):
    categoria=input("Ingrese categoria:")
    marca=input("Ingrese marca:")
    descripcion=input("Ingrese descripcion:")
    precio=float(input("Ingrese precio:"))
    cantidad=int(input("Ingrese cantidad:"))


    nuevo = Producto(id,categoria,marca,descripcion,precio,cantidad)
    db_producto[id] =  nuevo

    print("####### registro correcto ########")

def mostrarProductos():
    print("####### Productos ############")
    for key, value in db_producto.items():
        print(key,"=",value.verProducto())


def actualizarProducto(id):
    objetoUpdate=db_producto.get(id)
    categoria=input("Ingrese categoria:")
    marca=input("Ingrese marca:")
    descripcion=input("Ingrese descripcion:")
    precio=float(input("Ingrese precio:"))
    cantidad=int(input("Ingrese cantidad:"))
    objetoUpdate.categoria=categoria
    objetoUpdate.marca=marca
    objetoUpdate.descripcion=descripcion
    objetoUpdate.precio=precio
    objetoUpdate.cantidad=cantidad
    print("####### se ha actualizado un producto ########")
def eliminarProducto(id):
   db_producto.pop(id)
   print("####### se ha eliminado un producto ########")

ejecutar=True
while ejecutar:
    print("Bienvenidos al registro de productos")
    print("Precione la tecla * i * si desea Insertar un producto: ")
    print("Precione la tecla * a * si desea Actualizar un producto: ")
    print("Precione la tecla * e * si desea Eliminar un producto: ")
    print("Precione la tecla * m * si desea Mostrar los productos: ")
    accion = input("Ingrese aqui la operacion: ")

    if accion == "i":
        print("####### Nuevo Producto ############")
        contador_producto+=1
        nuevoProducto(contador_producto)
        mostrarProductos()
    elif accion == "a":
        print("####### Editar Producto ############")
        mostrarProductos()
        actualizarProducto(int(input("Ingrese el id: del producto a editar")))
        mostrarProductos()
    elif accion == "e":
        print("####### Eliminar Producto ############")
        mostrarProductos()
        eliminarProducto(int(input("Ingrese el id: del producto a eliminar")))
        mostrarProductos()
    elif accion == "m":
        
        mostrarProductos()                  
 



    valor=input("Desea registrar otro o terminar s/t: ")    
    if valor=="t":
        ejecutar=False
        print("fin del programa")    





