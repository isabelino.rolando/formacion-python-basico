#elementos unicos,no se admiten duplicados,
#elementos desordenados
#sus elementos son inmutables

s = set( [5,8,6,414,8] )
print(type(s))
print(s)
#metodo add(elemento) para añadir elementos al set
s.add(7)
print("add():",s)
#metodo remove(elemento) elimina un elemento dado
s.remove(414)
print("remove():",s)
#borra elementos metodo discard, si pasamos elementos no encontrados no pasa nada
s.discard(766)
print("discard()",s)
#metodo pop elimina un lemento de forma aleatorio
s.pop()
print("pop():",s)
#clear() elimina todos los elementos del set
s.clear()
print("clear()",s)
