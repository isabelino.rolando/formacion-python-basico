
class Color:
    rojo=""
    verde=""
    azul=""
    def __init__(self,rojo,verde,azul):
        self.rojo = rojo
        self.verde = verde
        self.azul = azul

    def mostrarDatos(self,obj):
        return f"{obj} - rojo: {self.rojo}, verde: {self.verde}, azul:{self.azul}"


negro = Color(0,0,0)
azul = Color(0,60,255)
print(negro)

#creo un diccionario y agrego los datos del objeto color
#__dict__ es un objeto de mapeo para almacenar atributos de escritura de tipo colleccion
colores_dict = negro.__dict__

print(colores_dict)
dict_objetos=dict([])
dict_objetos['negro'] = negro 
dict_objetos['azul'] = azul

print(dict_objetos['negro'].mostrarDatos('negro'))
print(dict_objetos['azul'].mostrarDatos('azul'))

        