
datos=dict([
    ('Nombre','Sara'),
    ('Edad',27),
    ('DNI', "A32322")
]) 

print(datos)

coleccion= dict([
  ('Nombre','Carlos'),#representa a un solo dato key:Nombre, value: Carlos
  ('Edad',30),#representa a un solo dato key:Edad, value: 30
  ('DNI',"BA8372") #representa a un solo dato key:DNI, value: BA8372 
])

print(coleccion)

#funcion get(key) para acceder a elemntos 
print("get()", coleccion.get("DNI") )

#metodo items() devuelve una lista con todos los keys y values del diccionario
it = coleccion.items()
print("items()",it)

#el metodo keys() devuelve una lista con todas las keys del diccionario
k = coleccion.keys()
print("keys():",k)

#metodo values() devuelve una lista con todos los values o valores
valores = coleccion.values()
print("values():",valores)

#metodo pop() elimina el elemento,de la key que se pasa como parametro
# y retorna el valor asociado a la key

#valor_borrado = coleccion.pop('DNI')
#print("pop():",coleccion)
#print(f"pop(): el elemento borrado es:{valor_borrado}") 

#popitem() elimina el ultimo registro
coleccion.popitem()
print(coleccion)

#update() actualiza un diccionario dado otro
coleccion.update(datos)
print(coleccion)
#Insertar nuevo elemento dentro de la coleccion 
coleccion['Direccion']="Calle Olvido 55"
print(coleccion)

#Actualizar
coleccion['Direccion']="Calle Estrella 22"
print(coleccion)

