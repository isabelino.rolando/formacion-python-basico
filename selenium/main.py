from selenium import webdriver

#abrir un navegador 
driver = webdriver.Chrome()
#abrir una direccion url especifica
driver.get("https://www.youtube.com")

#captura de titulo de una pagina web
title = driver.title
print("titulo: ",title)

#cierro el navegador
driver.quit()