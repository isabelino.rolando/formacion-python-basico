import os #para acceder a archivos internos de mi sistema operativo
import shutil #administrar archivos internos de mi sistema operativo

carpetaDescarga="C://Users//dell//Downloads//"
carpetaDocumentos="C://Users//dell//Documents//prueba_movida_python//"
carpetaEscritorio="C://Users//dell//Desktop//"
#guardo en get_files todos los archivos que tengo en descargas
get_files = os.listdir(carpetaDescarga)

for i in get_files:
    path, extension = os.path.splitext(carpetaDescarga + i)
    print(path)

    if extension in [".pptx"]:
        #la sentencia y metodo para mover archivos
        shutil.move(carpetaDescarga + i,carpetaDocumentos)

    if extension in [".TXT"]:
        shutil.move(carpetaDescarga + i, carpetaEscritorio)