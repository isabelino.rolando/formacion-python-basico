#escribir json
import json
myJson={
    'nombre':'Juan',
    'apellido':'Conde',
    'edad': 40
}

#pasar todo el json a str
j = json.dumps(myJson)

with open('lectura_archivos/mijson.json','w') as f:
    f.write(j)