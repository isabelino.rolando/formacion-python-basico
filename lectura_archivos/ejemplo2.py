#escribir csv
import csv

clientes = [
    ['Maria','Pérez','mp@email.com','Madrid'],
    ['Juan','Perales','jp@email.com','Asturias'],
    ['Carlos','Ramos','cr@email.com','Valencia'],
    ['Pedro','Ruiz','pr@email.com','Barcelona'],
    ['Luis','Alcaraz','la@email.com','Sevilla']
]

with open('lectura_archivos/clientes.csv','w',newline='',encoding='UTF-8') as file:
    datos = csv.writer(file,delimiter=',')
    datos.writerows( clientes )