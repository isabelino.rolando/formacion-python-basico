#funcion para saber el tipo de variable type()

#enteros int


numero = 12
print(numero)
print(type(numero))

#decimales float
numeroDecimal = 0.10088
print(numeroDecimal)
print(type(numeroDecimal))

#numeros complejos
a = 1 + 3j
b = 4 + 1j
print( a+b )

#tipos string o cadena str
s = "Esto es una cadena"
print(s)
print(type(s))

l = 'Esto es otra cadena'
print(l)
print(type(l))

#añadir caracteres dentro una cadena
t = "Esto es una comilla doble \" dentro \"  de una cadena"
print(t)

#añadir un salto de linea \n
p = "Esto es un salto de linea \n esto es la segunda linea"
print(p)

#concatenar string

num = 5
cadena = "El numero es: " + str( num )
print(cadena)

num2 = 6.9548
#cadena2 = "El resultado es: %d" %8 
#%s para variables de tipo String
#%i variable de tipo entera
#%f variable de tipo float, si queremos el resultado con dos decimales %.2f


cadena2 = "El resultado es: y el numero es %.2f" % num2 
print("concatenacion con %",cadena2)



#tambien podemos concatenar utilizando format()
cadena3 = "Los numeros son {a} y {b}".format(a = 20, b = 30)
print(cadena3)

#funciones con cadenas

cadena4 = "Esto es Una cadena en el CURSO DE python CURSO DE python CURSO DE python"
print( "lower: "+cadena4.lower() )
print( "upper: "+cadena4.upper() )
print( "title: "+cadena4.title() )
print( "swapcase: "+cadena4.swapcase() )
print( "capitalize: "+cadena4.capitalize() )

cadena5 = "CURSO DE python"

print("metodo count: " + str( cadena4.count(cadena5) ) )


txt = "Comany"
valor = txt.isalnum()
print("metodo isalnum:"+ str(valor) )

lista1 = ['1','2','3','4']
guion = "-"

print( guion.join(lista1) )
#split sin delimitador
cadena6= "Rapidos corren los carros"
print( cadena6.split() )
#split con delimitador
cadena6= "Rapidos corren, los carros"
print( cadena6.split(",") )

#metodo strip
cadena7="Programacion Python"
print(cadena7.strip("n"))
#metodo zfill
cadena8="Pruebas de codigo"
print( cadena8.zfill(20) ) 

#metodo len( aqui va la cadena a contar ) para medir la logitud de una cadena
longitud = len(cadena8)
print(longitud)


#Tipos de variables boleanos o logicos Bolean True o False

value = True
print(value)

value2 = False
print(value2)
print("evaluacion de expresiones")
print( 1 > 0 )#true
print( 1 <= 0 )#false
print( 8 == 8)#true

print("funcion bool")
print( bool("hola"))#true
print( bool( [] ))#false


#concantenacion con f, a partir de python 3.6 para adelante
nombre="Jose"
edad=30
estatura=1.70

a = f"Mi nombre es {nombre}, tengo {edad} años y mido {estatura} mts de altura "
print(a)





