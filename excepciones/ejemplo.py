#exception
try:
    d = 2/""
except Exception as e:    
    print("hay un error: ", type(e) )

print("Aqui termina el programa")
#personalizamos excepciones
try:
    c = 5/0
except TypeError:
    print("Error: Problema de tipos")
except ZeroDivisionError:
    print("No se puede dividir entre cero!")   

print("Aqui termina el programa por segunda vez")

#podemos agregar bloque de codigo
#el else se ejecutara si no ha ocurrido ninguna excepcion
try:
    y=2/1
except:
    print("Ingresa al exception")
else:        
    print("Ingresa al else")

print("Aqui termina el programa por tercera vez")

#podemos agregar bloque de codigo
#finally ejecuta dicho bloque haya o no haya habido una excepcion
try:
    l=10/2
except:
    print("Ingresa al exception")
finally:
    print("Aqui es el finally, se ejecuta el bloque")