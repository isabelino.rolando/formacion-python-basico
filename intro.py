#salida de informacion con la funcion print()
#python no requiere declaracion de tipo de variable
#python no soporta uso del caracter $ para declarar variable
#python no requiere cerrar una linea de codigo con ;(punto y coma)
x = 10;
y = 15
#si necesito tener multiples lineas de codigo en la misma linea
m = 10 ; n = 15 ; suma = m+n;print(suma)

#python no requiere uso de {} para estructuras de control
saludo =  "hola mundo python"
print(saludo)

#cometario en una sola linea de codigo
'''
comentario en varias lineas de codigo
con comillas simples
'''

"""
comentario en varias lineas de codigo
con comillas dobles
"""

#identacion: mover un bloque de condigo hacia la derecha con tabuladores o espacios,
#para tener el margen o visualizacion correcta del bloque de codigo

if True:
    #bloque de codigo de la condicional if
    print("true")